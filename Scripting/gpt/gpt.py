def chat():
	import openai
	import translate as tr
	import pyttsx3

	engine = pyttsx3.init()
	voices = engine.getProperty('voices')
	engine.setProperty('voice', 'spanish')

	openai.api_key = "sk-WzYeKus3iL9OOIp7LftGEOJANVW6MPhqaLxuyhUf"

	print('Esto es una conversación con una asistente de inteligencia artificial. Es servicial, creativa, inteligente, y amigable.\n\nHuman: Hola, cómo estás?\nAI: Cómo te puedo ayudar hoy?\nHuman:')

	query = tr.voz()

	response = openai.Completion.create(engine="davinci", prompt=f'The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: Hello, who are you?\nAI: I am an AI created by OpenAI. How can I help you today?\nHuman: {query}\n', max_tokens=150)

	res = response['choices'][0]['text']

	respuesta = tr.esp(res)

	print(respuesta)

	engine.say(respuesta)
	engine.runAndWait()


	
