
def voz():
	import speech_recognition as sr

	r = sr.Recognizer() 

	with sr.Microphone() as source:
	    print('Te escucho: ')
	    audio = r.listen(source)
	 
	    try:
	        text = r.recognize_google(audio)
	        print('You said: {}'.format(text))
	    except:
	        print('Lo siento, no te escuché')

	print(text)

	from googletrans import Translator
	translator = Translator()

	trans = translator.translate(text, dest='en')
	print(trans.text)

	return trans.text

def esp(response):
	from googletrans import Translator
	translator = Translator()

	trans = translator.translate(response, dest='es')

	return trans.text
