# Kwin script

## Aquí encontrarás la forma de obtener los factores de kwin a aprtir de un export de adverity para importar las bases de manera manual al datastream de adverity

el datastream que se está utilizando es: https://ktbo.datatap.adverity.com/ktbo/kwin-central-tendencies-96/

subirás el export resultsfinalDB2020.csv resultado de ejecutar el script llamado kwin_eval.r para ejecutarlo es necesario tener en el mismo directorio un export de todos los datastreams desde el 2020.

para installar r en la ec2 se usó la instrucción:

```
sudo amazon-linux-extras install R4
```
