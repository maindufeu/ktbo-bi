import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime
from datetime import timedelta
from datetime import date
import subprocess

today = date.today()
d2 = timedelta(days = 105)
startDate = str(today-d2)
startDate = f'{startDate}'
print('startdate:',startDate)
df = pd.read_csv('Campaigns/staging.csv')
df['keybind'] = df['platform_id'].astype(str) + '-' + (df['daily'].str.split('-')).str.join('')
df['created_at'] = datetime.now()
df['daily'] = pd.to_datetime(df['daily']).dt.date
print(df)

ENDPOINT="database-1.cbrebg97dmdd.us-east-2.rds.amazonaws.com"
PORT="3306"
USR="admin"
REGION="us-east-2"
DBNAME="database-1"
PWD = 'Schopenhauer2021*'
DB = 'KTBOKUBEcampaigns'

connection_string = f'mysql+pymysql://{USR}:{PWD}@{ENDPOINT}:{PORT}/{DB}'
db = create_engine(connection_string)

try:
    db = create_engine(connection_string)
    df.to_sql(con=db, name='staging', if_exists='replace')
    query = '''SELECT * from staging limit 1'''
    result = db.engine.execute(query).fetchall()
    pr = pd.DataFrame(result)
    print(pr)

    clean_stg = '''create table new_stg as (
    with select_max as (
    select keybind, max(platform_cost) as cost, MAX(engagements)as eng , count(*) as rep from staging
    group by keybind)
    select stg.* from staging as stg
    left join select_max
    on select_max.keybind = stg.keybind
    and select_max.cost = stg.platform_cost
    and select_max.eng = stg.engagements
    where stg.keybind is not null);'''
    result = db.engine.execute(clean_stg)

    clean2_stg = '''drop table if exists staging;'''
    result = db.engine.execute(clean2_stg)

    stg_step2 = '''    create table staging as (select * from new_stg);'''
    result = db.engine.execute(stg_step2)

    stg_step3 = '''drop table if exists new_stg;'''
    result = db.engine.execute(stg_step3)

    duplicates_tt = '''drop table if exists duplicates;'''
    result = db.engine.execute(duplicates_tt)

    create_d = '''create table duplicates as(
    select STG.keybind as d_keybind, STG.platform_id from staging as STG
    JOIN factual_item as TGT
    on STG.keybind = TGT.keybind
    where TGT.keybind is not  null)'''
    result = db.engine.execute(create_d)

    factual_delete = ''' DELETE FROM factual_item
    where keybind in (select d_keybind from duplicates);'''
    result = db.engine.execute(factual_delete)

    factual_insert = '''INSERT INTO factual_item
    SELECT platform_id, clicks, video_views, video100, engagements, impressions, platform_cost, daily, created_at, keybind FROM staging'''
    result = db.engine.execute(factual_insert)

    query = '''SELECT * from factual_item limit 10'''
    result = db.engine.execute(query).fetchall()
    pr = pd.DataFrame(result)
    print('factual',pr)

    dimension_delete = ''' DELETE FROM dimension
    where platform_id in (select platform_id from duplicates);'''
    result = db.engine.execute(dimension_delete)

    dimension_insert = '''    insert into dimension
    SELECT  campaign_name, campaign_label, platform, brand, initiative, platform_id, datasource FROM staging groupby platform_id;'''
    result = db.engine.execute(dimension_insert)

    query = '''SELECT * from dimension limit 10'''
    result = db.engine.execute(query).fetchall()
    pr = pd.DataFrame(result)
    print('dimensional',pr)

    drop_duplicates = '''DROP TABLE duplicates;'''
    result = db.engine.execute(drop_duplicates)

    df = pd.read_sql_query(f'SELECT * from kwindaily where daily > {startDate}', con = db)
    df = df.rename(columns={'campaign_name': 'Campaign Name', 'campaign_label':'Campaign Label','platform':'Platform','initiative':'Initiative','brand':'Brand','platform_cost':'Platform Cost', 'impressions':'Impressions', 'clicks':'Clicks', 'engagements':'Engagements', 'video_views':'Video Views', 'video100':'Video Completions', 'daily':'Daily', 'datasource':'Datasource'})
    #get_view = f'SELECT * from kwindaily where daily > {startDate}'
    #print(get_view)
    #result = db.engine.execute(get_view).fetchall()
    #df = pd.DataFrame(result)
    df.to_csv('KWIN/kwin90.csv', index = False)

except Exception as e:
    print("Database connection failed due to {}".format(e))

subprocess.call('git add .', shell=True)
subprocess.call('git commit -m "campaigns kwinunified update"', shell=True)
subprocess.call('git push', shell=True)
