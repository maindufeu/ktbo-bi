from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import fnmatch
import io
import json
from googleapiclient.http import MediaIoBaseDownload
import pandas as pd
import subprocess

# this is the scope permission you can review more about this on: https://developers.google.com/drive/api/v3/about-auth?hl=en
SCOPES = ['https://www.googleapis.com/auth/drive']
#you need to have the token.json files created by the quickstart: https://developers.google.com/drive/api/v3/quickstart/python
creds = Credentials.from_authorized_user_file('token.json', SCOPES)
service = build('drive', 'v3', credentials=creds)
mediacomFolder_id = '1FXHM94WqnMa18pnz-ysuXhVMbJyzaPsR'

query = f"mimeType = 'application/vnd.google-apps.folder' and \'{mediacomFolder_id}\' in parents"
results = service.files().list(q=query,
                                fields="nextPageToken, files(id, name)").execute()

def fileinFolder(folder_id, folder_name):
    month_list =[]
    monthid_list = []
    query = f" \'{folder_id}\' in parents"
    results = service.files().list(q=query,
                                    fields='nextPageToken, files(id, name)').execute()

    subitems = results.get('files', [])
    for subitem in subitems:
        if "Other" in subitem['name']:
            query = f" \'{subitem['id']}\' in parents"
            results = service.files().list(q=query,
                                            fields='nextPageToken, files(id, name)').execute()
            files = results.get('files', [])
            for file in files:
                if "Offline" in file['name']:
                    print(subitem['name'])
                    month_list.append(file['name'])
                    monthid_list.append(file['id'])
                    print(u'{0} ({1})'.format(file['name'], file['id']))
    month_dict = {monthid_list[i]: month_list[i] for i in range(len(month_list))}
    return month_dict

def file_down(file_id, file_name):
    request = service.files().get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print ("Download %d%%." % int(status.progress() * 100))

    fh.seek(0)

    with open (os.path.join('Campaigns/drive_offline/', file_name), 'wb') as f:
        f.write(fh.read())
        f.close()


n = 1
ff_list =[]
months = results.get('files', [])
for month in months:
    if fileinFolder(month['id'], month['name']) != {}:
        print(n, month['name'])
        ff_list.append(fileinFolder(month['id'], month['name']))

down = int(input())-1
for i in ff_list[down]:
    print(i)
    file_down(i, ff_list[down][i])
pattern = '*.xlsx'
for i in os.listdir('Campaigns/drive_offline'):
    if fnmatch.fnmatch(i, pattern):
        print(i)
        df =pd.read_excel(f'Campaigns/drive_offline/{i}', usecols = ['Start Date', 'End Date', 'Month', 'Brand', 'Region', 'Country', 'Initiative', 'Campaign', 'Platform', 'Touchpoint', 'Format', 'Goal', 'Investment'])
        #df['day'] = df['Month'].dt.strptime(s, '%d %B, %Y').strftime('%Y-%m-%d')
        df.to_csv(f'''Campaigns/drive_offline/result/{'_'.join(i.split('_')[1:6])}.csv''')

subprocess.call('aws s3 sync Campaigns/drive_offline/result/ s3://othercampaigns/offline/', shell=True)
